FROM fredrikhgrelland/alpine-jdk11-openssl
WORKDIR /app
COPY build/libs/*.jar ./statistics.jar

CMD ["java", "-jar", "statistics.jar"]