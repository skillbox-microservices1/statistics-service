package com.autum.statistics.persistance;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.business.EventRepository;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import com.autum.statistics.persistance.entity.Event;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;


@Component
@AllArgsConstructor
public class EventRepositoryImpl implements EventRepository {

    private EventRepositoryDb repositoryDb;
    private Mapper mapper;


    @Override
    public EventListDto getUserEvents(String userUuid, EventDto.EventType type, Pageable pageable) {
        var page = repositoryDb.findAllByUserUuidAndType(userUuid, type.name(), pageable);
        return map(page);
    }

    @Override
    public EventListDto getObjEvents(String objectUuid, EventDto.EventType type, Pageable pageable) {
        var page = repositoryDb.findAllByObjectUuidAndType(objectUuid, type.name(), pageable);
        return map(page);
    }

    @Override
    public EventDto saveEvent(EventDto dto) {
        var event = mapper.map(dto, Event.class);
        event = repositoryDb.save(event);
        return mapper.map(event, EventDto.class);
    }

    private EventListDto map(Page<Event> page) {
        return EventListDto.builder()
                .totalCount(page.getTotalElements())
                .events(page.stream()
                        .map(event -> mapper.map(event, EventDto.class))
                        .collect(Collectors.toList())
                )
                .build();
    }
}