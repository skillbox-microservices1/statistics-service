package com.autum.statistics.persistance.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Getter
@Setter
@Document("events")
public class Event {

    @Id
    private String id;
    private String userUuid;
    private String objectUuid;
    private String type;
    private LocalDateTime createdAt;
}