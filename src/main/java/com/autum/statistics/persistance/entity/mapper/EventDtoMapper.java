package com.autum.statistics.persistance.entity.mapper;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.infrastructure.mapstruct.MainMapper;
import com.autum.statistics.persistance.entity.Event;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface EventDtoMapper extends MainMapper<Event, EventDto> {
}