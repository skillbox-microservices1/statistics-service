package com.autum.statistics.persistance;

import com.autum.statistics.persistance.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepositoryDb extends MongoRepository<Event, String> {

    Page<Event> findAllByUserUuidAndType(String userUuid, String type, Pageable pageable);

    Page<Event> findAllByObjectUuidAndType(String objectUuid, String type, Pageable pageable);
}