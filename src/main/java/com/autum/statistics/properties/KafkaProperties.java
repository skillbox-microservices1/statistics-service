package com.autum.statistics.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Getter
@Setter
@Component
public class KafkaProperties {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    @Value(value = "${spring.kafka.sasl.mechanism}")
    private String mechanism;
    @Value(value = "${spring.kafka.sasl.jaas.config}")
    private String jaasConfig;
    @Value(value = "${spring.kafka.security.protocol}")
    private String protocol;
}