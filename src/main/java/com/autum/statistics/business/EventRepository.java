package com.autum.statistics.business;

import org.springframework.data.domain.Pageable;


public interface EventRepository {

    EventListDto getUserEvents(String userUuid, EventDto.EventType type, Pageable pageable);

    EventListDto getObjEvents(String objectUuid, EventDto.EventType type, Pageable pageable);

    EventDto saveEvent(EventDto dto);
}
