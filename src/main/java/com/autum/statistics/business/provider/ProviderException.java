package com.autum.statistics.business.provider;

import lombok.Getter;


@Getter
public class ProviderException extends RuntimeException {

    private final Integer code;
    private final String body;

    public ProviderException(Integer code, String body) {
        this.code = code;
        this.body = body;
    }
}