package com.autum.statistics.business.provider;

public interface UserProvider {

    UserDto getUser(String accessToken) throws ProviderException;
}