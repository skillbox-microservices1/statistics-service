package com.autum.statistics.business;

import org.springframework.data.domain.Pageable;


public interface EventService {

    EventListDto getEvents(String uuid, EventDto.EventType type, Pageable pageable);

    EventListDto getUserEventsByLogin(String accessToken, EventDto.EventType type, Pageable pageable);

    void addEvent(EventDto event);
}