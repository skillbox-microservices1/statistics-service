package com.autum.statistics.business;

import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
public class EventListDto {

    private long totalCount;
    private List<EventDto> events;
}