package com.autum.statistics.business;

import com.autum.statistics.business.provider.UserProvider;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class EventServiceImpl implements EventService {

    private EventRepository repository;
    private UserProvider userProvider;


    public EventListDto getEvents(String uuid, EventDto.EventType type, Pageable pageable) {
        switch (type) {
            case COMMENT_LIKE:
            case PUBLICATION_LIKE:
                //TODO нужн проверка публикаций или комментария
                return repository.getObjEvents(uuid, type, pageable);
            case SUBSCRIPTIONS:
                return repository.getUserEvents(uuid, type, pageable);
            default:
                throw new IllegalArgumentException("Unexpected EventType: " + type);
        }
    }

    @Override
    public EventListDto getUserEventsByLogin(String accessToken, EventDto.EventType type, Pageable pageable) {
        var user = userProvider.getUser(accessToken);
//        var sort = Sort.by(Sort.Direction.DESC, "createdAt");
        return repository.getUserEvents(user.getUuid(), type, pageable);
    }

    @Override
    public void addEvent(EventDto dto) {
        repository.saveEvent(dto);
    }
}