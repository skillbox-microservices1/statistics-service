package com.autum.statistics.business;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;


@Getter
@Setter
public class EventDto {

    private String id;
    private String userUuid;
    private String objectUuid;
    private EventType type;
    private LocalDateTime createdAt;

    public enum EventType {
        PUBLICATION_LIKE,
        COMMENT_LIKE,
        SUBSCRIPTIONS
    }
}