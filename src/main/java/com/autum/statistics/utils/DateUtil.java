package com.autum.statistics.utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;


public class DateUtil {

    public static Long getMillisFromLocalDateTime(LocalDateTime time) {
        return time.toInstant(ZoneOffset.UTC)
                .toEpochMilli();
    }
}