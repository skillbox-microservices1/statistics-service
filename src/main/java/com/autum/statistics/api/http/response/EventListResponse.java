package com.autum.statistics.api.http.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class EventListResponse {

    @Schema(description = "The total number of entities in the database")
    private long totalCount;
    @Schema(description = "List of events")
    private List<EventResponse> items;
}