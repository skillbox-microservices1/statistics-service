package com.autum.statistics.api.http.controller;

import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.business.EventService;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.autum.statistics.api.http.util.JwtUtil.USER;
import static com.autum.statistics.business.EventDto.EventType.SUBSCRIPTIONS;


@Tag(name = "VIEWER ROLE API")
@Secured(USER)
@Controller
@RequestMapping(value = "/api/v1/statistics")
@AllArgsConstructor
public class UserController {

    private EventService service;
    private Mapper mapper;


    @Operation(summary = "Getting a subscriptions")
    @GetMapping("/events/subscriptions")
    @ResponseBody
    public EventListResponse getSubscriptions(@AuthenticationPrincipal Jwt jwt, Pageable pageable) {
        var result = service.getUserEventsByLogin(jwt.getTokenValue(), SUBSCRIPTIONS, pageable);
        return mapper.map(result, EventListResponse.class);
    }
}