package com.autum.statistics.api.http.error;


public enum ErrorCode {

    INVALID_MEDIA_TYPE,
    ACCESS_DENIED,
    INTERNAL_ERROR
}