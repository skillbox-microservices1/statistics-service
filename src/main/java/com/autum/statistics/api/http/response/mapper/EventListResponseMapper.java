package com.autum.statistics.api.http.response.mapper;

import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.api.http.response.EventResponse;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.infrastructure.mapstruct.MainMapper;
import com.autum.statistics.infrastructure.mapstruct.converter.TimeConverter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface EventListResponseMapper extends MainMapper<EventListDto, EventListResponse>, TimeConverter {

    @Override
    @Mapping(target = "items", source = "events")
    EventListResponse map(EventListDto request);

    @Mapping(target = "createdAt", source = "createdAt", qualifiedByName = "LocalDateTimeToLong")
    EventResponse eventDtoToEventResponse(EventDto userDto);
}