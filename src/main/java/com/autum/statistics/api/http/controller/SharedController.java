package com.autum.statistics.api.http.controller;

import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.business.EventService;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.autum.statistics.business.EventDto.EventType;


@Tag(name = "ANONYM OR SHARED ROLE API")
@Controller
@RequestMapping(value = "/api/v1/statistics")
@AllArgsConstructor
public class SharedController {

    private EventService service;
    private Mapper mapper;


    @Operation(summary = "Getting a publication likes")
    @GetMapping("/events/{type:publication|comment}/{uuid}/likes")
    @ResponseBody
    public EventListResponse getLikes(@PathVariable String uuid, @PathVariable String type, Pageable pageable) {
        var eventType = EventType.valueOf(type.toUpperCase() + "_LIKE");
        var eventList = service.getEvents(uuid, eventType, pageable);
        return mapper.map(eventList, EventListResponse.class);
    }
}