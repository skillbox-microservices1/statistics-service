package com.autum.statistics.api.http.response.mapper;

import com.autum.statistics.api.http.response.EventResponse;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.infrastructure.mapstruct.MainMapper;
import com.autum.statistics.infrastructure.mapstruct.converter.TimeConverter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface EventResponseMapper extends MainMapper<EventDto, EventResponse>, TimeConverter {

    @Override
    @Mapping(target = "createdAt", source = "createdAt", qualifiedByName = "LocalDateTimeToLong")
    EventResponse map(EventDto dto);
}