package com.autum.statistics.api.http.response;

import com.autum.statistics.api.http.error.ErrorCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;


@Getter
@Setter
@NoArgsConstructor
public class ErrorResponse {

    @Schema(description = "Error code")
    private ErrorCode errorCode;
    @Schema(description = "Error description")
    private String message;
    @Schema(description = "Additional error Details")
    private Map<String, String> details;

    public ErrorResponse(ErrorCode code, String msg) {
        errorCode = code;
        message = msg;
    }
}