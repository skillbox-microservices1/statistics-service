package com.autum.statistics.api.http.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class EventResponse {

    @Schema(description = "Event ID")
    private String id;
    @Schema(description = "ID of the user who triggered the event")
    private String userUuid;
    @Schema(description = "ID of the entity that the user interacted with")
    private String objectUuid;
    @Schema(description = "Type of event")
    private String type;
    @Schema(description = "Creation time")
    private Long createdAt;
}