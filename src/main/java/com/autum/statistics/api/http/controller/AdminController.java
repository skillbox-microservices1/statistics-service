package com.autum.statistics.api.http.controller;

import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.business.EventService;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import static com.autum.statistics.api.http.util.JwtUtil.ADMIN;
import static com.autum.statistics.business.EventDto.EventType.SUBSCRIPTIONS;


@Tag(name = "ADMIN ROLE API")
@Secured(ADMIN)
@Controller
@RequestMapping(value = "/api/v1/statistics")
@AllArgsConstructor
public class AdminController {

    private EventService service;
    private Mapper mapper;


    @Operation(summary = "Getting a subscriptions")
    @GetMapping("/events/user/{uuid}/subscriptions")
    @ResponseBody
    public EventListResponse getSubscriptions(@PathVariable String uuid, Pageable pageable) {
        var result = service.getEvents(uuid, SUBSCRIPTIONS, pageable);
        return mapper.map(result, EventListResponse.class);
    }
}