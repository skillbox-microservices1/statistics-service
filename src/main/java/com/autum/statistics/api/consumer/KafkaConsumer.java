package com.autum.statistics.api.consumer;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventService;
import lombok.AllArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class KafkaConsumer {

    private final EventService service;

    //TODO topic и group брать из properties
    @KafkaListener(topics = "events", groupId = "VGD-32432")
    public void listen(EventDto message) {
        service.addEvent(message);
    }
}