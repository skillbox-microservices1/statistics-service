package com.autum.statistics.infrastructure.mapstruct.converter;

import com.autum.statistics.utils.DateUtil;
import org.mapstruct.Named;

import java.time.LocalDateTime;


public interface TimeConverter {

    @Named("LocalDateTimeToLong")
    default Long localDateTimeToLong(LocalDateTime time) {
        return time == null ? null : DateUtil.getMillisFromLocalDateTime(time);
    }
}