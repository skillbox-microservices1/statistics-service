package com.autum.statistics.infrastructure.mapstruct;

public interface MainMapper<T, E> {
    E map(T t);
}