import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.utils.DateUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegressTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/statistics";
    }

    @Test
    public void userPublicationLike() throws InterruptedException {

        var eventTime = LocalDateTime.now();
        var userUuid = "111I3233RYDSWRN21342CCEREWR24323";
        var commentUuid = "NOPI3233RYDSWRN21342CCEREWR24323";

        var event = new EventDto();
        event.setUserUuid(userUuid);
        event.setObjectUuid(commentUuid);
        event.setCreatedAt(eventTime);
        event.setType(EventDto.EventType.COMMENT_LIKE);
        var key = UUID.randomUUID().toString();
        Thread.sleep(3000);
        kafkaTemplate.send("events", key, event);

        await()
                .pollInterval(Duration.ofSeconds(3))
                .atMost(6, SECONDS)
                .untilAsserted(() -> {
                    var url = baseUrl + "/events/comment/" + commentUuid + "/likes";
                    var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);

                    assertEquals(HttpStatus.OK, response.getStatusCode());
                    var body = response.getBody();
                    assertNotNull(body);
                    assertEquals(1L, body.getTotalCount());
                    assertNotNull(body.getItems());
                    assertEquals(1L, body.getItems().size());

                    var eventRes = body.getItems().get(0);
                    assertNotNull(eventRes);
                    assertNotNull(eventRes.getId());
                    assertEquals(userUuid, eventRes.getUserUuid());
                    assertEquals(commentUuid, eventRes.getObjectUuid());
                    assertEquals("COMMENT_LIKE", eventRes.getType());
                    var date = DateUtil.getMillisFromLocalDateTime(eventTime);
                    assertEquals(date, eventRes.getCreatedAt());
                });
    }
}