import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;


public abstract class AbstractTest {

    protected static final String TRANSFER_PROTOCOL = "HTTPS";

    protected static final Integer KEYCLOAK_PORT = 8403;
    protected static final Integer KEYCLOAK_PORT_EXTERNAL = 8443;

    protected static final Integer MONGO_PORT = 27017;
    private static final String MONGO_USER = "mongo";
    private static final String MONGO_PASSWORD = "123";
    private static final String MONGO_DB = "Statistics";

    protected static final Integer KAFKA_PORT = 9094;

    protected static final GenericContainer KEYCLOAK_CONTAINER = new FixedHostPortGenericContainer<>("quay.io/keycloak/keycloak:23.0.7")
            .withFixedExposedPort(KEYCLOAK_PORT, KEYCLOAK_PORT_EXTERNAL)
            .withCommand(
                    "-Dkeycloak.migration.action=import",
                    "-Dkeycloak.migration.provider=dir",
                    "-Dkeycloak.migration.dir=/opt/keycloak",
                    "start-dev"
            )
            .withEnv("KEYCLOAK_ADMIN", "admin")
            .withEnv("KEYCLOAK_ADMIN_PASSWORD", "admin")
            .withEnv("KC_HTTPS_CERTIFICATE_FILE", "/opt/keycloak/keycloak-crt.pem")
            .withEnv("KC_HTTPS_CERTIFICATE_KEY_FILE", "/opt/keycloak/keycloak-key.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak-social-network-realm.json"), "/opt/keycloak/keycloak-social-network-realm.json")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak_cert.pem"), "/opt/keycloak/keycloak-crt.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak_key.pem"), "/opt/keycloak/keycloak-key.pem");

    protected static final GenericContainer MONGO_CONTAINER = new FixedHostPortGenericContainer<>("mongo")
            .withFixedExposedPort(MONGO_PORT, MONGO_PORT)
            .withCommand("mongod --config /etc/mongo.conf")
            .withEnv("MONGO_INITDB_ROOT_USERNAME", MONGO_USER)
            .withEnv("MONGO_INITDB_ROOT_PASSWORD", MONGO_PASSWORD)
            .withEnv("MONGO_INITDB_DATABASE", MONGO_DB)
            .withCopyFileToContainer(MountableFile.forClasspathResource("mongo_ca.pem"), "/etc/ssl/ca.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("mongo.conf"), "/etc/mongo.conf")
            .withCopyFileToContainer(MountableFile.forClasspathResource("mongodb.pem"), "/etc/ssl/server.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("mongo-init.js"), "/docker-entrypoint-initdb.d/mongo-init.js");

    private static final String KAFKA_KEYSTORE_PATH = "/opt/bitnami/kafka/config/certs/kafka.keystore.jks";
    private static final String KAFKA_TRUSTSTORE_PATH = "/opt/bitnami/kafka/config/certs/kafka.truststore.jks";

    protected final static GenericContainer KAFKA_CONTAINER = new FixedHostPortGenericContainer<>("bitnami/kafka:3.7")
            .withFixedExposedPort(KAFKA_PORT, KAFKA_PORT)
            .withEnv("KAFKA_CFG_PROCESS_ROLES", "broker,controller")
            .withEnv("KAFKA_CFG_NODE_ID", "0")
            .withEnv("KAFKA_CFG_CONTROLLER_QUORUM_VOTERS", "0@localhost:9093")
            .withEnv("KAFKA_CFG_AUTO_CREATE_TOPICS_ENABLE", "true")
            .withEnv("KAFKA_CFG_CONTROLLER_LISTENER_NAMES", "CONTROLLER")
            .withEnv("KAFKA_CFG_INTER_BROKER_LISTENER_NAME", "INTER_BROKER")
            .withEnv("KAFKA_CFG_LISTENERS", "INTER_BROKER://:9092,CONTROLLER://:9093,EXTERNAL://:9094")
            .withEnv("KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP", "CONTROLLER:PLAINTEXT,INTER_BROKER:PLAINTEXT,EXTERNAL:SASL_SSL")
            .withEnv("KAFKA_CFG_ADVERTISED_LISTENERS", "INTER_BROKER://localhost:9092,EXTERNAL://localhost:9094")
            .withEnv("KAFKA_CFG_SASL_ENABLED_MECHANISMS", "PLAIN")
            .withEnv("KAFKA_CFG_SECURITY_PROTOCOL", "SASL_SSL")
            .withEnv("KAFKA_CFG_SSL_KEYSTORE_LOCATION", KAFKA_KEYSTORE_PATH)
            .withEnv("KAFKA_CFG_SSL_TRUSTSTORE_LOCATION", KAFKA_TRUSTSTORE_PATH)
            .withEnv("KAFKA_CFG_SSL_TRUSTSTORE_PASSWORD", "secret")
            .withEnv("KAFKA_CFG_SSL_KEYSTORE_PASSWORD", "secret")
            .withEnv("KAFKA_CFG_SSL_KEY_PASSWORD", "secret")
            .withEnv("KAFKA_CFG_LISTENER_NAME_EXTERNAL_PLAIN_SASL_JAAS_CONFIG", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"foobar\" password=\"qwerty\" user_foobar=\"qwerty\";")
            .withCopyFileToContainer(MountableFile.forClasspathResource("kafka.keystore.jks"), KAFKA_KEYSTORE_PATH)
            .withCopyFileToContainer(MountableFile.forClasspathResource("kafka.truststore.jks"), KAFKA_TRUSTSTORE_PATH)
            .waitingFor(Wait.forLogMessage(".*Kafka Server started.*", 1));

    protected static final MockServerContainer MOCK_HTTP_SERVER = new MockServerContainer(DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion()));

    @BeforeAll
    public static void setUp() {
        try {
            KAFKA_CONTAINER.start();
            KEYCLOAK_CONTAINER.start();
            MONGO_CONTAINER.start();
            MOCK_HTTP_SERVER.start();
//            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DynamicPropertySource
    protected static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("user-config.url", () -> "http://" + MOCK_HTTP_SERVER.getHost() + ":" + MOCK_HTTP_SERVER.getServerPort());
    }

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    protected String kcAddress;

    protected String getAccessToken(String username, TestRestTemplate client) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        var map = new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", "123");
        map.add("grant_type", "password");
        map.add("client_id", "social-network-client");

        var request = new HttpEntity<>(map, headers);

        var response = client.postForEntity(kcAddress + "/protocol/openid-connect/token", request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                var jsonObject = new JSONObject(response.getBody());
                return jsonObject.getString("access_token");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("Failed to get access token. Status code: " + response.getStatusCodeValue());
        }
    }
}