package api.controller;

import com.autum.statistics.api.http.controller.UserController;
import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.api.http.response.EventResponse;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.business.EventServiceImpl;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import com.autum.statistics.utils.DateUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.jwt.Jwt;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.autum.statistics.business.EventDto.EventType.SUBSCRIPTIONS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class UserControllerTest {

    @Test
    public void getSubscriptions() {
        var pageable = Pageable.ofSize(10);
        var type = SUBSCRIPTIONS;
        var jwt = getToken();

        var time = LocalDateTime.now();

        var service = Mockito.mock(EventServiceImpl.class);
        var mapper = Mockito.mock(Mapper.class);

        var controller = new UserController(service, mapper);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("OIOP-IPOI-POIP");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid("VFHTYRIDSE12345VBCJDERD45645FDF2");
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        var eventResponse = new EventResponse();
        eventResponse.setId(eventDto.getId());
        eventResponse.setType(eventDto.getType().name());
        eventResponse.setUserUuid(eventDto.getUserUuid());
        eventResponse.setObjectUuid(eventDto.getObjectUuid());
        eventResponse.setCreatedAt(DateUtil.getMillisFromLocalDateTime(eventDto.getCreatedAt()));

        var eventListResponse = new EventListResponse();
        eventListResponse.setTotalCount(1);
        eventListResponse.setItems(List.of(eventResponse));

        when(service.getUserEventsByLogin(jwt.getTokenValue(), type, pageable)).thenReturn(eventListDto);
        when(mapper.map(eventListDto, EventListResponse.class)).thenReturn(eventListResponse);

        var result = controller.getSubscriptions(jwt, pageable);
        assertEquals(1, result.getTotalCount());
        assertEquals(1, result.getItems().size());

        var itemResult = result.getItems().get(0);
        assertEquals(eventResponse.getId(), itemResult.getId());
        assertEquals(eventResponse.getType(), itemResult.getType());
        assertEquals(eventResponse.getUserUuid(), itemResult.getUserUuid());
        assertEquals(eventResponse.getObjectUuid(), itemResult.getObjectUuid());
        assertEquals(eventResponse.getCreatedAt(), itemResult.getCreatedAt());

        verify(service, times(1)).getUserEventsByLogin(jwt.getTokenValue(), type, pageable);
        verify(mapper, times(1)).map(eventListDto, EventListResponse.class);
    }

    private Jwt getToken() {
        var issuedAt = Instant.ofEpochMilli(1712083574L);
        var expiredAt = Instant.ofEpochMilli(1798397174L);

        var token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ1WEVZaGw4SklOMVpaM0ZuX2tybG82MF9qUkVQRjlvSnRuOE9CYzZUX1BRIn0.eyJleHAiOjE3OTgzOTcxNzQsImlhdCI6MTcxMjA4MzU3NCwianRpIjoiMjQ1MDczYzktNGZiNS00NzhlLThhODItYjM4NTM5NjgzZDFiIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4NTU2L3JlYWxtcy9zb2NpYWwtbmV0d29yay1hcHAiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiM2ZhMzQwNDQtNTU4Ni00NTczLTg3OGItY2Q1N2RhNmI4MDY2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoic29jaWFsLW5ldHdvcmstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6ImQyNDBlZTkzLWFmYzEtNGFmMC1hNTQ2LTllYjYzOGY1M2MzYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiVVNFUlNfVklFV0VSIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLXNvY2lhbC1uZXR3b3JrLWFwcCIsIkJPQVJEX1ZJRVdFUiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsInNpZCI6ImQyNDBlZTkzLWFmYzEtNGFmMC1hNTQ2LTllYjYzOGY1M2MzYyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiSGFuIFNvbG8iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ4YW4iLCJnaXZlbl9uYW1lIjoiSGFuIiwiZmFtaWx5X25hbWUiOiJTb2xvIiwiZW1haWwiOiJlbWFpbEBnbWFpbC5jb20ifQ.B5FhyDOhDPgHZOBbfOvMcJrrAui4rJWgGSB1uYDQsOnTXLWn9kBK5_gXEFqQ4jyDc1AX5onfK9N52c-WzjfSIPneyYUZGbjHZR8yL69M0sAJdBJ692ASFgC8wivigtZEivRRyz54lqzwCcb4yw1JN4yU4h1Z116O2OCAm32KpZrQ4Rasivke6o0SANy5k3VJCliHZZ3RF3ZvXPjiLJ5a_KtL-nW_tHg6OPHTbqmlWiKGnBGzp4MmCKTvFqxJZXpbvwYIz2Z4LgJ_t8TR1rJCXcC_0ZMd8y4qT574FI5Z5Q8VMIa733GLVq6khvA501zpLWHg_dhNBXAvwQN-nTueug";
        var headers = new HashMap<String, Object>();
        headers.put("alg", "RS256");
        headers.put("typ", "JWT");
        headers.put("kid", "uXEYhl8JIN1ZZ3Fn_krlo60_jREPF9oJtn8OBc6T_PQ");
        var claims = new HashMap<String, Object>();
        claims.put("exp", "1798397174");
        claims.put("iat", "1798397174");
        claims.put("jti", "245073c9-4fb5-478e-8a82-b38539683d1b");
        claims.put("iss", "http://localhost:8556/realms/social-network-app");
        claims.put("aud", "account");
        claims.put("sub", "3fa34044-5586-4573-878b-cd57da6b8066");
        claims.put("typ", "Bearer");
        claims.put("azp", "social-network-client");
        claims.put("session_state", "d240ee93-afc1-4af0-a546-9eb638f53c3c");
        claims.put("acr", "1");
        claims.put("scope", "email profile");
        claims.put("sid", "d240ee93-afc1-4af0-a546-9eb638f53c3c");
        claims.put("email_verified", "true");
        claims.put("name", "Han Solo");
        claims.put("preferred_username", "xan");
        claims.put("given_name", "Han");
        claims.put("family_name", "Solo");
        claims.put("email", "email@gmail.com");

        var rolesAccount = List.of("manage-account", "manage-account-links", "view-profile");
        var accountRoles = Map.of("roles", rolesAccount);
        var resourceAccess = Map.of("account", accountRoles);
        claims.put("resource_access", resourceAccess);

        var rolesRealm = List.of("offline_access", "USERS_VIEWER", "BOARD_VIEWER", "uma_authorization", "default-roles-social-network-app");
        var realmAccess = Map.of("roles", rolesRealm);
        claims.put("realm_access", realmAccess);

        var allowedOrigin = List.of("/*");
        claims.put("allowed-origins", allowedOrigin);

        return new Jwt(token, issuedAt, expiredAt, headers, claims);
    }
}