package api.controller;

import com.autum.statistics.api.http.controller.AdminController;
import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.api.http.response.EventResponse;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.business.EventServiceImpl;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import com.autum.statistics.utils.DateUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static com.autum.statistics.business.EventDto.EventType.SUBSCRIPTIONS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;


public class AdminControllerTest {

    @Test
    public void getSubscriptions() {
        var uuid = "VFHTYRIDSE12345VBCJDERD45645FDF2";
        var pageable = Pageable.ofSize(10);
        var type = SUBSCRIPTIONS;

        var time = LocalDateTime.now();

        var service = Mockito.mock(EventServiceImpl.class);
        var mapper = Mockito.mock(Mapper.class);

        var controller = new AdminController(service, mapper);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("1111-1111-1111-1111");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid(uuid);
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        var eventResponse = new EventResponse();
        eventResponse.setId(eventDto.getId());
        eventResponse.setType(eventDto.getType().name());
        eventResponse.setUserUuid(eventDto.getUserUuid());
        eventResponse.setObjectUuid(eventDto.getObjectUuid());
        eventResponse.setCreatedAt(DateUtil.getMillisFromLocalDateTime(eventDto.getCreatedAt()));

        var eventListResponse = new EventListResponse();
        eventListResponse.setTotalCount(1);
        eventListResponse.setItems(List.of(eventResponse));

        when(service.getEvents(uuid, type, pageable)).thenReturn(eventListDto);
        when(mapper.map(eventListDto, EventListResponse.class)).thenReturn(eventListResponse);

        var result = controller.getSubscriptions(uuid, pageable);
        assertEquals(1, result.getTotalCount());
        assertEquals(1, result.getItems().size());

        var itemResult = result.getItems().get(0);
        assertEquals(eventResponse.getId(), itemResult.getId());
        assertEquals(eventResponse.getType(), itemResult.getType());
        assertEquals(eventResponse.getUserUuid(), itemResult.getUserUuid());
        assertEquals(eventResponse.getObjectUuid(), itemResult.getObjectUuid());
        assertEquals(eventResponse.getCreatedAt(), itemResult.getCreatedAt());

        verify(service, times(1)).getEvents(uuid, type, pageable);
        verify(mapper, times(1)).map(eventListDto, EventListResponse.class);
    }
}