package api.error;

import com.autum.statistics.api.http.error.CustomExceptionHandler;
import com.autum.statistics.api.http.response.ErrorResponse;
import com.autum.statistics.business.provider.ProviderException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;

import java.util.Locale;

import static com.autum.statistics.api.http.error.ErrorCode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;

public class ExceptionHandlerTest {

    private MessageSource messageSource;
    private CustomExceptionHandler exceptionHandler;

    @BeforeEach
    void setup() {
        messageSource = mock(MessageSource.class);
        exceptionHandler = new CustomExceptionHandler(messageSource);
    }

    @Test
    void accessDenied() {
        var exception = new AccessDeniedException(null,null);
        var locale = new Locale("en");
        var msg = "The role does not match, access is denied";
        var errorCode = ACCESS_DENIED;

        when(messageSource.getMessage(errorCode.name(), null, locale)).thenReturn(msg);

        var result = exceptionHandler.accessDenied(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void testHandleHttpMediaTypeNotAcceptableException() {
        var exception = new HttpMediaTypeNotAcceptableException("TEST EXCEPTION MSG");
        var locale = new Locale("en");
        var msg = "Incorrect media type";
        var errorCode = INVALID_MEDIA_TYPE;

        when(messageSource.getMessage(eq(errorCode.name()), isNull(), eq(locale))).thenReturn(msg);

        var result = exceptionHandler.handleHttpMediaTypeNotAcceptableException(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void testUnexpectedException() {
        var exception = new RuntimeException(new Throwable("TEST EXCEPTION MSG"));
        var locale = new Locale("en");
        var msg = "Internal server error";
        var errorCode = INTERNAL_ERROR;

        when(messageSource.getMessage(eq(errorCode.name()), isNull(), eq(locale))).thenReturn(msg);

        var result = exceptionHandler.unexpectedException(exception, locale);

        assertEquals(errorCode, result.getErrorCode());
        assertEquals(msg, result.getMessage());
        assertNull(result.getDetails());

        verify(messageSource, times(1)).getMessage(errorCode.name(), null, locale);
    }

    @Test
    void providerException_unexpectedException() {
        var body = "TEST EXCEPTION MSG";
        var status = HttpStatus.BAD_REQUEST;
        var exception = new ProviderException(status.value(), body);

        var result = exceptionHandler.providerException(exception);

        assertEquals(body, result.getBody());
        assertEquals(status, result.getStatusCode());
    }

    @Test
    public void errorResponse() {

        var errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(INTERNAL_ERROR);
        errorResponse.setMessage("Internal error");
        errorResponse.setDetails(null);

        assertEquals(INTERNAL_ERROR, errorResponse.getErrorCode());
        assertEquals("Internal error", errorResponse.getMessage());
        assertNull(errorResponse.getDetails());
    }
}
