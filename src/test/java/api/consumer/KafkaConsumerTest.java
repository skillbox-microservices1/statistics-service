package api.consumer;

import com.autum.statistics.api.consumer.KafkaConsumer;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;


public class KafkaConsumerTest {


    @Test
    void listenTest() {

        var service = Mockito.mock(EventServiceImpl.class);

        var consumer = new KafkaConsumer(service);

        var eventDto = new EventDto();
        eventDto.setType(EventDto.EventType.COMMENT_LIKE);
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid("VJFO123312DS23ERZTOPE3KFPOEKFEPW");
        eventDto.setCreatedAt(LocalDateTime.now());

        doNothing().when(service).addEvent(eventDto);

        consumer.listen(eventDto);

        verify(service, times(1)).addEvent(eventDto);
    }
}