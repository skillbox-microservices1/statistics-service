package mapstruct;

import com.autum.statistics.api.http.response.mapper.EventResponseMapperImpl;
import com.autum.statistics.utils.DateUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


class ConverterTest {

    @Test
    void localDateTimeToLong() {
        var time = LocalDateTime.now();
        var mapper = new EventResponseMapperImpl();
        var expected = DateUtil.getMillisFromLocalDateTime(time);
        var result = mapper.localDateTimeToLong(time);
        assertEquals(expected, result);
    }

    @Test
    void localDateTimeToLong_null() {
        var mapper = new EventResponseMapperImpl();
        var result = mapper.localDateTimeToLong(null);
        assertNull(result);
    }
}