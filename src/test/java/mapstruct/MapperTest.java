package mapstruct;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;

class MapperTest {

    @Test
    public void null_result() {
        var mapper = new Mapper(null);
        var result = mapper.map(null, EventDto.class);
        assertNull(result);
    }
}