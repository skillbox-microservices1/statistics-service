package config;

import com.autum.statistics.config.KafkaConfig;
import com.autum.statistics.config.SwaggerConfig;
import com.autum.statistics.config.SystemConfig;
import com.autum.statistics.config.WebConfig;
import com.autum.statistics.properties.KafkaProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


//TODO у конфига проверки только на null
class ConfigTest {

    @Test
    void restTemplate() {
        var config = new WebConfig();
        var result = config.restTemplate();
        Assertions.assertNotNull(result);
    }

    @Test
    void messageSource() {
        var config = new WebConfig();
        var result = config.messageSource();
        Assertions.assertNotNull(result);
    }

    @Test
    void localeResolver() {
        var config = new WebConfig();
        var result = config.localeResolver();
        Assertions.assertNotNull(result);
    }

    @Test
    void logFilter() {
        var config = new WebConfig();
        var result = config.logFilter();
        Assertions.assertNotNull(result);
    }

    @Test
    void swaggerApi() {
        var config = new SwaggerConfig();
        var result = config.api();
        Assertions.assertNotNull(result);
    }

    @Test
    void gson() {
        var config = new SystemConfig();
        var result = config.gson();
        Assertions.assertNotNull(result);
    }

    @Test
    void producerFactory() {
        var properties = new KafkaProperties();
        properties.setBootstrapServers("localhost:9094");
        properties.setProtocol("SASL_SSL");
        properties.setMechanism("PLAIN");
        properties.setJaasConfig("org.apache.kafka.common.security.scram.ScramLoginModule required username=\"foobar\" password=\"qwerty\";");

        var config = new KafkaConfig(properties);
        var result = config.producerFactory();
        Assertions.assertNotNull(result);
    }

    @Test
    void kafkaTemplate() {
        var properties = new KafkaProperties();
        properties.setBootstrapServers("localhost:9094");
        properties.setProtocol("SASL_SSL");
        properties.setMechanism("PLAIN");
        properties.setJaasConfig("org.apache.kafka.common.security.scram.ScramLoginModule required username=\"foobar\" password=\"qwerty\";");

        var config = new KafkaConfig(properties);
        var result = config.kafkaTemplate(config.producerFactory());
        Assertions.assertNotNull(result);
    }

    @Test
    void consumerFactory() {
        var properties = new KafkaProperties();
        properties.setBootstrapServers("localhost:9094");
        properties.setProtocol("SASL_SSL");
        properties.setMechanism("PLAIN");
        properties.setJaasConfig("org.apache.kafka.common.security.scram.ScramLoginModule required username=\"foobar\" password=\"qwerty\";");

        var config = new KafkaConfig(properties);
        var result = config.consumerFactory();
        Assertions.assertNotNull(result);
    }

    @Test
    void kafkaListenerContainerFactory() {
        var properties = new KafkaProperties();
        properties.setBootstrapServers("localhost:9094");
        properties.setProtocol("SASL_SSL");
        properties.setMechanism("PLAIN");
        properties.setJaasConfig("org.apache.kafka.common.security.scram.ScramLoginModule required username=\"foobar\" password=\"qwerty\";");

        var config = new KafkaConfig(properties);
        var result = config.kafkaListenerContainerFactory(config.consumerFactory(),
                config.kafkaTemplate(config.producerFactory()));
        Assertions.assertNotNull(result);
    }
}