import com.autum.statistics.utils.DateUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static java.time.ZoneOffset.UTC;


class DateUtilTest {

    @Test
    void getMillisFromLocalDateTime() {
        var time = LocalDateTime.now();
        var expected = time.toInstant(UTC).toEpochMilli();
        var result = DateUtil.getMillisFromLocalDateTime(time);
        Assertions.assertEquals(expected, result);
    }
}