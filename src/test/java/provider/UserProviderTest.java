package provider;

import com.autum.statistics.business.provider.ProviderException;
import com.autum.statistics.business.provider.UserDto;
import com.autum.statistics.properties.UserProperties;
import com.autum.statistics.provider.UserProviderImpl;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


class UserProviderTest {


    @Test
    void getUser_returnsUserDto() {

        var userProperties = mock(UserProperties.class);
        var restTemplate = mock(RestTemplate.class);
        var gson = new Gson();

        var userProvider = new UserProviderImpl(restTemplate, userProperties, gson);

        var accessToken = "testToken";
        var url = "https://localhost";

        var userDto = new UserDto();
        userDto.setUuid("GJKJDFIEUNDUIEWF98787987FHEAIUFH");
        userDto.setFirstName("First");
        userDto.setLastName("Last");
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex("MALE");
        userDto.setSubscriptions(0L);
        userDto.setSubscribers(0L);
        userDto.setMiddleName("Middle");

        var city = new UserDto.CityDto();
        city.setUuid("VBFHRYDSCV89743GYTR89UIR453FGT45");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);

        var skill = new UserDto.SkillDto();
        skill.setUuid("NBDFIH12324EIOFHOI23432FHEAOFHOI");
        skill.setName("java");
        skill.setLevel(10);

        userDto.setSkills(List.of(skill));
        userDto.setAbout("about");

        var responseEntity = new ResponseEntity<>(gson.toJson(userDto), HttpStatus.OK);

        when(userProperties.getUrl()).thenReturn(url);
        when(restTemplate.exchange(eq(url + "/api/v1/users/yourself"), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
                .thenReturn(responseEntity);

        var result = userProvider.getUser(accessToken);

        assertEquals(userDto.getUuid(), result.getUuid());
        assertEquals(userDto.getFirstName(), result.getFirstName());
        assertEquals(userDto.getLastName(), result.getLastName());
        assertEquals(userDto.getMiddleName(), result.getMiddleName());
        assertEquals(userDto.getAbout(), result.getAbout());
        assertEquals(userDto.getSex(), result.getSex());
        assertEquals(userDto.getStatus(), result.getStatus());
        assertEquals(userDto.getSubscriptions(), result.getSubscriptions());
        assertEquals(userDto.getSubscribers(), result.getSubscribers());
        assertEquals(1, userDto.getSkills().size());

        var skillDtoActual = result.getSkills().get(0);
        assertEquals(skill.getUuid(), skillDtoActual.getUuid());
        assertEquals(skill.getName(), skillDtoActual.getName());
        assertEquals(skill.getLevel(), skillDtoActual.getLevel());

        assertEquals(city.getUuid(), result.getCity().getUuid());
        assertEquals(city.getName(), result.getCity().getName());
        assertEquals(city.getCountry(), result.getCity().getCountry());
        assertEquals(city.getRegion(), result.getCity().getRegion());

        assertNull(result.getAvatarUrl());

        verify(userProperties, times(1)).getUrl();
        verify(restTemplate, times(1)).exchange(eq(url + "/api/v1/users/yourself"), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class));
    }

    @Test
    void getUser_throwsProviderException() {

        var userProperties = mock(UserProperties.class);
        var restTemplate = mock(RestTemplate.class);

        var userProvider = new UserProviderImpl(restTemplate, userProperties, null);

        var accessToken = "testToken";
        var url = "testUrl";

        var errorBody = "Error message";
        var responseEntity = new ResponseEntity<>(errorBody, HttpStatus.BAD_REQUEST);

        when(userProperties.getUrl()).thenReturn(url);
        when(restTemplate.exchange(eq(url + "/api/v1/users/yourself"), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
                .thenReturn(responseEntity);

        try {
            userProvider.getUser(accessToken);
        } catch (ProviderException e) {
            assertEquals(HttpStatus.BAD_REQUEST.value(), e.getCode());
            assertEquals(errorBody, e.getBody());
        }
    }
}