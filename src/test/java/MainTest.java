import com.autum.statistics.StatisticsApplication;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;
import org.springframework.context.support.StaticApplicationContext;


class MainTest {

    @Test
    void mainTest() {
        try (MockedStatic<SpringApplication> spring = Mockito.mockStatic(SpringApplication.class)) {
            var cxt = new StaticApplicationContext();
            var arg = new String[]{"Test"};
            spring.when(() -> SpringApplication.run(StatisticsApplication.class, arg)).thenReturn(cxt);
            StatisticsApplication.main(arg);
            spring.verify(() -> SpringApplication.run(StatisticsApplication.class, arg));
        }
    }
}