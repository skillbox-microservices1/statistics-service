package business;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.business.EventListDto;
import com.autum.statistics.business.EventServiceImpl;
import com.autum.statistics.business.provider.UserDto;
import com.autum.statistics.persistance.EventRepositoryImpl;
import com.autum.statistics.provider.UserProviderImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static com.autum.statistics.business.EventDto.EventType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class EventServiceTest {

    @Test
    void getEvents_commentsLike() {
        var uuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var type = COMMENT_LIKE;
        var time = LocalDateTime.now();

        var repo = Mockito.mock(EventRepositoryImpl.class);

        var service = new EventServiceImpl(repo, null);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("1111-1111-1111-1111");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid(uuid);
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        when(repo.getObjEvents(uuid, type, pageable)).thenReturn(eventListDto);

        var result = service.getEvents(uuid, type, pageable);
        assertEquals(eventListDto, result);

        verify(repo, times(1)).getObjEvents(uuid, type, pageable);
    }

    @Test
    void getEvents_publicationLike() {
        var uuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var type = PUBLICATION_LIKE;
        var time = LocalDateTime.now();

        var repo = Mockito.mock(EventRepositoryImpl.class);

        var service = new EventServiceImpl(repo, null);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("1111-1111-1111-1111");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid(uuid);
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        when(repo.getObjEvents(uuid, type, pageable)).thenReturn(eventListDto);

        var result = service.getEvents(uuid, type, pageable);
        assertEquals(eventListDto, result);

        verify(repo, times(1)).getObjEvents(uuid, type, pageable);
    }

    @Test
    void getEvents_subscription() {
        var uuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var type = SUBSCRIPTIONS;
        var time = LocalDateTime.now();

        var repo = Mockito.mock(EventRepositoryImpl.class);

        var service = new EventServiceImpl(repo, null);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("1111-1111-1111-1111");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid(uuid);
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        when(repo.getUserEvents(uuid, type, pageable)).thenReturn(eventListDto);

        var result = service.getEvents(uuid, type, pageable);
        assertEquals(eventListDto, result);

        verify(repo, times(1)).getUserEvents(uuid, type, pageable);
    }

    @Test
    void getUserEventsByLogin() {
        var accessToken = "vnrkkeihoi32iojo2ii";
        var userUuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var type = SUBSCRIPTIONS;
        var time = LocalDateTime.now();

        var repo = Mockito.mock(EventRepositoryImpl.class);
        var userProvider = Mockito.mock(UserProviderImpl.class);

        var service = new EventServiceImpl(repo, userProvider);

        var userDto = new UserDto();
        userDto.setUuid(userUuid);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setId("1111-1111-1111-1111");
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid(userUuid);
        eventDto.setCreatedAt(time);

        var eventListDto = EventListDto.builder()
                .totalCount(1)
                .events(List.of(eventDto))
                .build();

        when(userProvider.getUser(accessToken)).thenReturn(userDto);
        when(repo.getUserEvents(userUuid, type, pageable)).thenReturn(eventListDto);

        var result = service.getUserEventsByLogin(accessToken, type, pageable);
        assertEquals(eventListDto, result);

        verify(repo, times(1)).getUserEvents(userUuid, type, pageable);
        verify(userProvider, times(1)).getUser(accessToken);
    }

    @Test
    void addEvent() {
        var repo = Mockito.mock(EventRepositoryImpl.class);

        var service = new EventServiceImpl(repo, null);

        var eventDto = new EventDto();
        eventDto.setType(EventDto.EventType.COMMENT_LIKE);
        eventDto.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        eventDto.setUserUuid("VFHTYRIDSE12345VBCJDERD45645FDF2");
        eventDto.setCreatedAt(LocalDateTime.now());

        var savedEventDto = new EventDto();
        savedEventDto.setType(eventDto.getType());
        savedEventDto.setId("1111-1111-1111-1111");
        savedEventDto.setObjectUuid(eventDto.getObjectUuid());
        savedEventDto.setUserUuid(eventDto.getUserUuid());
        savedEventDto.setCreatedAt(eventDto.getCreatedAt());

        when(repo.saveEvent(eventDto)).thenReturn(savedEventDto);

        service.addEvent(eventDto);

        verify(repo, times(1)).saveEvent(eventDto);
    }
}