package properties;

import com.autum.statistics.properties.UserProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PropertiesTest {

    @Test
    void userTest() {

        var url = "http://autum.com";

        var properties = new UserProperties();
        properties.setUrl(url);

        assertEquals(url, properties.getUrl());
    }
}