package persistance;

import com.autum.statistics.business.EventDto;
import com.autum.statistics.infrastructure.mapstruct.Mapper;
import com.autum.statistics.persistance.EventRepositoryDb;
import com.autum.statistics.persistance.EventRepositoryImpl;
import com.autum.statistics.persistance.entity.Event;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static com.autum.statistics.business.EventDto.EventType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


public class EventRepositoryTest {

    @Test
    public void getUserEvents() {
        var uuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var time = LocalDateTime.now();
        var type = SUBSCRIPTIONS;

        var repoDb = Mockito.mock(EventRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);

        var repo = new EventRepositoryImpl(repoDb, mapper);

        var event1 = new Event();
        event1.setType(type.name());
        event1.setId("1111-1111-1111-1111");
        event1.setObjectUuid("111PRDSAZ1234CUI98REWE78943FEFEF");
        event1.setUserUuid(uuid);
        event1.setCreatedAt(time);

        var event2 = new Event();
        event2.setType(type.name());
        event2.setId("2222-2222-2222-2222");
        event2.setObjectUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        event2.setUserUuid(uuid);
        event2.setCreatedAt(time);

        var eventDto1 = new EventDto();
        eventDto1.setType(type);
        eventDto1.setId(event1.getId());
        eventDto1.setObjectUuid(event1.getObjectUuid());
        eventDto1.setUserUuid(event1.getUserUuid());
        eventDto1.setCreatedAt(event1.getCreatedAt());

        var eventDto2 = new EventDto();
        eventDto2.setType(type);
        eventDto2.setId(event2.getId());
        eventDto2.setObjectUuid(event2.getObjectUuid());
        eventDto2.setUserUuid(event2.getUserUuid());
        eventDto2.setCreatedAt(event2.getCreatedAt());

        var page = new PageImpl<>(List.of(event1, event2), pageable, 2);

        when(repoDb.findAllByUserUuidAndType(uuid, type.name(), pageable)).thenReturn(page);
        when(mapper.map(event1, EventDto.class)).thenReturn(eventDto1);
        when(mapper.map(event2, EventDto.class)).thenReturn(eventDto2);

        var result = repo.getUserEvents(uuid, type, pageable);
        assertNotNull(result);
        assertNotNull(result.getEvents());
        assertEquals(2, result.getTotalCount());
        assertEquals(2, result.getEvents().size());

        var eventResult1 = result.getEvents().get(0);
        assertEquals(event1.getId(), eventResult1.getId());
        assertEquals(event1.getUserUuid(), eventResult1.getUserUuid());
        assertEquals(event1.getObjectUuid(), eventResult1.getObjectUuid());
        assertEquals(event1.getType(), eventResult1.getType().name());
        assertEquals(event1.getCreatedAt(), eventResult1.getCreatedAt());

        var eventResult2 = result.getEvents().get(1);
        assertEquals(event2.getId(), eventResult2.getId());
        assertEquals(event2.getUserUuid(), eventResult2.getUserUuid());
        assertEquals(event2.getObjectUuid(), eventResult2.getObjectUuid());
        assertEquals(event2.getType(), eventResult2.getType().name());
        assertEquals(event2.getCreatedAt(), eventResult2.getCreatedAt());

        verify(mapper, times(1)).map(event1, EventDto.class);
        verify(mapper, times(1)).map(event2, EventDto.class);
        verify(repoDb, times(1)).findAllByUserUuidAndType(uuid, type.name(), pageable);
    }

    @Test
    public void getObjEvents() {
        var uuid = "VJFDLOPIIOWEHF234324OIHFOEHFOWEI";
        var pageable = Pageable.ofSize(10);
        var time = LocalDateTime.now();
        var type = COMMENT_LIKE;

        var repoDb = Mockito.mock(EventRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);

        var repo = new EventRepositoryImpl(repoDb, mapper);

        var event1 = new Event();
        event1.setType(type.name());
        event1.setId("1111-1111-1111-1111");
        event1.setObjectUuid(uuid);
        event1.setUserUuid("111PRDSAZ1234CUI98REWE78943FEFEF");
        event1.setCreatedAt(time);

        var event2 = new Event();
        event2.setType(type.name());
        event2.setId("2222-2222-2222-2222");
        event2.setObjectUuid(uuid);
        event2.setUserUuid("VIOPRDSAZ1234CUI98REWE78943FEFEF");
        event2.setCreatedAt(time);

        var eventDto1 = new EventDto();
        eventDto1.setType(type);
        eventDto1.setId(event1.getId());
        eventDto1.setObjectUuid(event1.getObjectUuid());
        eventDto1.setUserUuid(event1.getUserUuid());
        eventDto1.setCreatedAt(event1.getCreatedAt());

        var eventDto2 = new EventDto();
        eventDto2.setType(type);
        eventDto2.setId(event2.getId());
        eventDto2.setObjectUuid(event2.getObjectUuid());
        eventDto2.setUserUuid(event2.getUserUuid());
        eventDto2.setCreatedAt(event2.getCreatedAt());

        var page = new PageImpl<>(List.of(event1, event2), pageable, 2);

        when(repoDb.findAllByObjectUuidAndType(uuid, type.name(), pageable)).thenReturn(page);
        when(mapper.map(event1, EventDto.class)).thenReturn(eventDto1);
        when(mapper.map(event2, EventDto.class)).thenReturn(eventDto2);

        var result = repo.getObjEvents(uuid, type, pageable);
        assertNotNull(result);
        assertNotNull(result.getEvents());
        assertEquals(2, result.getTotalCount());
        assertEquals(2, result.getEvents().size());

        var eventResult1 = result.getEvents().get(0);
        assertEquals(event1.getId(), eventResult1.getId());
        assertEquals(event1.getUserUuid(), eventResult1.getUserUuid());
        assertEquals(event1.getObjectUuid(), eventResult1.getObjectUuid());
        assertEquals(event1.getType(), eventResult1.getType().name());
        assertEquals(event1.getCreatedAt(), eventResult1.getCreatedAt());

        var eventResult2 = result.getEvents().get(1);
        assertEquals(event2.getId(), eventResult2.getId());
        assertEquals(event2.getUserUuid(), eventResult2.getUserUuid());
        assertEquals(event2.getObjectUuid(), eventResult2.getObjectUuid());
        assertEquals(event2.getType(), eventResult2.getType().name());
        assertEquals(event2.getCreatedAt(), eventResult2.getCreatedAt());

        verify(mapper, times(1)).map(event1, EventDto.class);
        verify(mapper, times(1)).map(event2, EventDto.class);
        verify(repoDb, times(1)).findAllByObjectUuidAndType(uuid, type.name(), pageable);
    }

    @Test
    public void saveEvent() {
        var type = PUBLICATION_LIKE;
        var time = LocalDateTime.now();

        var repoDb = Mockito.mock(EventRepositoryDb.class);
        var mapper = Mockito.mock(Mapper.class);

        var repo = new EventRepositoryImpl(repoDb, mapper);

        var event = new Event();
        event.setType(type.name());
        event.setObjectUuid("111PRDSAZ1234CUI98REWE78943FEFEF");
        event.setUserUuid("VJFDLOPIIOWEHF234324OIHFOEHFOWEI");
        event.setCreatedAt(time);

        var eventDto = new EventDto();
        eventDto.setType(type);
        eventDto.setObjectUuid(event.getObjectUuid());
        eventDto.setUserUuid(event.getUserUuid());
        eventDto.setCreatedAt(event.getCreatedAt());

        var eventSaved = new Event();
        eventSaved.setId("1111-1111-1111-1111");
        eventSaved.setType(type.name());
        eventSaved.setObjectUuid("111PRDSAZ1234CUI98REWE78943FEFEF");
        eventSaved.setUserUuid("VJFDLOPIIOWEHF234324OIHFOEHFOWEI");
        eventSaved.setCreatedAt(time);

        var eventDtoSaved = new EventDto();
        eventDtoSaved.setId(eventSaved.getId());
        eventDtoSaved.setType(type);
        eventDtoSaved.setObjectUuid(eventSaved.getObjectUuid());
        eventDtoSaved.setUserUuid(eventSaved.getUserUuid());
        eventDtoSaved.setCreatedAt(eventSaved.getCreatedAt());

        when(mapper.map(eventDto, Event.class)).thenReturn(event);
        when(mapper.map(eventSaved, EventDto.class)).thenReturn(eventDtoSaved);
        when(repoDb.save(event)).thenReturn(eventSaved);

        var result = repo.saveEvent(eventDto);

        assertEquals(eventDtoSaved, result);

        verify(mapper, times(1)).map(eventDto, Event.class);
        verify(mapper, times(1)).map(eventSaved, EventDto.class);
        verify(repoDb, times(1)).save(event);
    }
}