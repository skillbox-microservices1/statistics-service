import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.api.http.response.EventListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmokeTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;

    @Autowired
    private TestRestTemplate client;

    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/statistics";
    }

    @Test
    public void smoke() {
        var uuid = "442D88O3224FDSWIO34324FEFE324AEF";
        var url = baseUrl + "/events/publication/" + uuid + "/likes";
        var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(0L, body.getItems().size());
    }
}
