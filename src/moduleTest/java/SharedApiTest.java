import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.api.http.response.EventListResponse;
import config.SharedMongoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = SharedMongoConfig.class)
public class SharedApiTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;

    @Autowired
    private TestRestTemplate client;

    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/statistics";
    }

    @Test
    public void getPublicationLikes() {
        var uuid = "902DUIO3224FDSWIO34324FEFE324A09";
        var url = baseUrl + "/events/publication/" + uuid + "/likes";
        var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(1L, body.getItems().size());
        var event = body.getItems().get(0);
        assertNotNull(event);
        assertNotNull(event.getId());
        assertEquals("FG2DUIO3224FDSWIO34324FEFE324AEF", event.getUserUuid());
        assertEquals("902DUIO3224FDSWIO34324FEFE324A09", event.getObjectUuid());
        assertEquals("PUBLICATION_LIKE", event.getType());
        assertEquals(1720912262345L, event.getCreatedAt());
    }

    @Test
    public void getPublicationLikes_empty() {
        var uuid = "442D88O3224FDSWIO34324FEFE324AEF";
        var url = baseUrl + "/events/publication/" + uuid + "/likes";
        var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(0L, body.getItems().size());
    }

    @Test
    public void getCommentLikes() {
        var uuid = "CCCDUIOVV24FDS2IO34324FEFE324ANM";
        var url = baseUrl + "/events/comment/" + uuid + "/likes";
        var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(1L, body.getItems().size());
        var event = body.getItems().get(0);
        assertNotNull(event);
        assertNotNull(event.getId());
        assertEquals("111DUIO3224FDS2IO34324FEFE324AEF", event.getUserUuid());
        assertEquals("CCCDUIOVV24FDS2IO34324FEFE324ANM", event.getObjectUuid());
        assertEquals("COMMENT_LIKE", event.getType());
        assertEquals(1720912262345L, event.getCreatedAt());
    }

    @Test
    public void getCommentLikes_empty() {
        var uuid = "442D88O3224FDSWIO34324FEFE324AEF";
        var url = baseUrl + "/events/comment/" + uuid + "/likes";
        var response = client.exchange(url, HttpMethod.GET, null, EventListResponse.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(0L, body.getItems().size());
    }
}