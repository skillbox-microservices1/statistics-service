import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.api.http.response.EventListResponse;
import com.autum.statistics.business.provider.UserDto;
import com.google.gson.Gson;
import config.UserMongoConfig;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = UserMongoConfig.class)
public class UserApiTest extends AbstractTest {

    private final MockServerClient mockServerClient = new MockServerClient(MOCK_HTTP_SERVER.getHost(), MOCK_HTTP_SERVER.getServerPort());

    @LocalServerPort
    private int port;
    private String baseUrl;
    @Autowired
    private TestRestTemplate client;


    @PostConstruct
    public void init() {
        baseUrl = TRANSFER_PROTOCOL + "://localhost:" + port + "/api/v1/statistics";
    }

    @Test
    public void getSubscriptions() {
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var userDto = new UserDto();
        userDto.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");

        var city = new UserDto.CityDto();
        city.setUuid("11FDCV7890FDERE6709DEWDE324VB321");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var url = baseUrl + "/events/subscriptions";
        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.GET, httpEntity, EventListResponse.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(1L, body.getItems().size());
        var event = body.getItems().get(0);
        assertNotNull(event);
        assertNotNull(event.getId());
        assertEquals("RUFDCV7890FDERE6709DEWDE324VBFDF", event.getUserUuid());
        assertEquals("111DUIO3224FDS2IO34324FEFE324AEF", event.getObjectUuid());
        assertEquals("SUBSCRIPTIONS", event.getType());
        assertEquals(1720912262345L, event.getCreatedAt());
    }

    @Test
    public void getSubscriptions_empty() {
        var username = "autum";

        var accessToken = getAccessToken(username, client);
        var auth = "Bearer " + accessToken;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", auth);

        var userDto = new UserDto();
        userDto.setUuid("999DCV7890FDERE6709DEWDE324VBFDF");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");

        var city = new UserDto.CityDto();
        city.setUuid("11FDCV7890FDERE6709DEWDE324VB321");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex("MALE");
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var url = baseUrl + "/events/subscriptions";
        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.GET, httpEntity, EventListResponse.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(0L, body.getItems().size());
    }
}