package config;

import com.autum.statistics.persistance.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;


@TestConfiguration
public class ConsumerMongoConfig {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Bean
    public MongoDataInitializer mongoDataInitializer() {
        return new MongoDataInitializer();
    }

    public class MongoDataInitializer implements ApplicationListener<ApplicationReadyEvent> {

        @Override
        public void onApplicationEvent(ApplicationReadyEvent event) {
            mongoTemplate.dropCollection(Event.class);
        }
    }
}