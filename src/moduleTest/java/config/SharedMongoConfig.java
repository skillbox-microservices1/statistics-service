package config;

import com.autum.statistics.persistance.entity.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


@TestConfiguration
public class SharedMongoConfig {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Bean
    public MongoDataInitializer mongoDataInitializer() {
        return new MongoDataInitializer();
    }

    public class MongoDataInitializer implements ApplicationListener<ApplicationReadyEvent> {

        @Override
        public void onApplicationEvent(ApplicationReadyEvent event) {
            try {
                mongoTemplate.dropCollection(Event.class);
                Resource resource = new ClassPathResource("mongo/data-for-shared.json");
                Gson gson = new Gson();

                List<Event> events = gson.fromJson(new InputStreamReader(resource.getInputStream()), new TypeToken<List<Event>>() {
                }.getType());

                mongoTemplate.insertAll(events);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
