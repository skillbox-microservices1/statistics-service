import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.api.http.response.EventListResponse;
import config.AdminMongoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = AdminMongoConfig.class)
public class AdminApiTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;
    private HttpHeaders headersWithToken;
    @Autowired
    private TestRestTemplate client;

    @PostConstruct
    public void init() {
        headersWithToken = getHeadersWithAccessTokenAndJson();
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/statistics";
    }

    @Test
    public void getSubscriptions() {
        var userUUid = "8G2D88O3224FDSWIO34324FEFE324AEF";
        var url = baseUrl + "/events/user/" + userUUid + "/subscriptions";
        var httpEntity = new HttpEntity<>(null, headersWithToken);
        var response = client.exchange(url, HttpMethod.GET, httpEntity, EventListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(1L, body.getItems().size());
        var event = body.getItems().get(0);
        assertNotNull(event);
        assertNotNull(event.getId());
        assertEquals("8G2D88O3224FDSWIO34324FEFE324AEF", event.getUserUuid());
        assertEquals("111DUIO3224FDS2IO34324FEFE324AEF", event.getObjectUuid());
        assertEquals("SUBSCRIPTIONS", event.getType());
        assertEquals(1720912262345L, event.getCreatedAt());

    }

    @Test
    public void getSubscriptions_empty() {
        var userUUid = "442D88O3224FDSWIO34324FEFE324AEF";
        var url = baseUrl + "/events/user/" + userUUid + "/subscriptions";
        var httpEntity = new HttpEntity<>(null, headersWithToken);
        var response = client.exchange(url, HttpMethod.GET, httpEntity, EventListResponse.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(0L, body.getTotalCount());
        assertNotNull(body.getItems());
        assertEquals(0L, body.getItems().size());

    }

    private HttpHeaders getHeadersWithAccessTokenAndJson() {
        var accessToken = getAccessToken("admin", client);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + accessToken);
        return headers;
    }
}