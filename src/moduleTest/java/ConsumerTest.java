import com.autum.statistics.StatisticsApplication;
import com.autum.statistics.business.EventDto;
import com.autum.statistics.persistance.EventRepositoryDb;
import com.autum.statistics.utils.DateUtil;
import config.ConsumerMongoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(classes = StatisticsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = ConsumerMongoConfig.class)
public class ConsumerTest extends AbstractTest {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Autowired
    private EventRepositoryDb repositoryDb;

    @Test
    public void kafkaListenerTest() throws InterruptedException {
        var eventTime = LocalDateTime.now();
        var eventObjUuid = "MOJO34EJFIOWHGFIUWOEHRF32432FEFE";
        var userUuid = "NOPI3233RYDSWRN21342CCEREWR24323";

        var event = new EventDto();
        event.setUserUuid(userUuid);
        event.setObjectUuid(eventObjUuid);
        event.setCreatedAt(eventTime);
        event.setType(EventDto.EventType.COMMENT_LIKE);
        var key = UUID.randomUUID().toString();
        Thread.sleep(3000);
        kafkaTemplate.send("events", key, event);

        await()
                .pollInterval(Duration.ofSeconds(1))
                .atMost(5, SECONDS)
                .untilAsserted(() -> {
                    var pageable = Pageable.ofSize(1);
                    var page = repositoryDb.findAllByObjectUuidAndType(eventObjUuid, "COMMENT_LIKE", pageable);
                    var opt = page.get().findFirst();
                    assertThat(opt).isPresent();
                    var savedEvent = opt.get();
                    assertNotNull(savedEvent.getId());
                    assertEquals(userUuid, savedEvent.getUserUuid());
                    assertNotNull(savedEvent.getObjectUuid());
                    var date = DateUtil.getMillisFromLocalDateTime(eventTime);
                    var savedDate = DateUtil.getMillisFromLocalDateTime(savedEvent.getCreatedAt());
                    assertEquals(date, savedDate);
                });
    }
}